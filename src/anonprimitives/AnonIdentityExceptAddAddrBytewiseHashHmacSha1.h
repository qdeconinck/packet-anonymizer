//
// Copyright (C) 2016 Univesite catholique de Louvain (UCLouvain)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

#ifndef __ANON_IDENTITY_EXCEPT_ADD_ADDR_BYTEWISE_HASH_HMAC_SHA1_H
#define __ANON_IDENTITY_EXCEPT_ADD_ADDR_BYTEWISE_HASH_HMAC_SHA1_H

#include "AnonBytewiseHashHmacSha1.h"

class AnonIdentityExceptAddAddrBytewiseHashHmacSha1 : public AnonBytewiseHashHmacSha1 {

public:
							AnonIdentityExceptAddAddrBytewiseHashHmacSha1		(string key);
							~AnonIdentityExceptAddAddrBytewiseHashHmacSha1		();

protected:

	virtual ANON_RESULT					anonymize			(void* buf, unsigned int len);


};


#endif // __ANON_IDENTITY_EXCEPT_ADD_ADDR_BYTEWISE_HASH_HMAC_SHA1_H
