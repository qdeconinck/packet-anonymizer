//
// Copyright (C) 2016 Univesite catholique de Louvain (UCLouvain)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

#include "AnonIdentityExceptAddAddrBytewiseHashHmacSha1.h"

AnonIdentityExceptAddAddrBytewiseHashHmacSha1::AnonIdentityExceptAddAddrBytewiseHashHmacSha1 (string key) : AnonBytewiseHashHmacSha1(key)
{
}

AnonIdentityExceptAddAddrBytewiseHashHmacSha1::~AnonIdentityExceptAddAddrBytewiseHashHmacSha1 ()
{
}

AnonPrimitive::ANON_RESULT AnonIdentityExceptAddAddrBytewiseHashHmacSha1::anonymize (void* buf, unsigned int len)
{
	uint32_t i = 0;
	unsigned char* pnt;
	uint32_t length = 0;
	uint32_t subtype;
	bool ipv4 = false;
	uint32_t addressSize;

	while(i < len) {

		pnt	= (unsigned char*) buf + i;

		if (*pnt == 30) {
			// MPTCP option
			//std::cout << "Option value: " << (unsigned int)*pnt << std::endl;

			pnt = (unsigned char *) buf + i + 1;
			length = *pnt;
			pnt = (unsigned char *) buf + i + 2;
			subtype = (unsigned int) (*pnt & 0xF0) >> 4;
			if (subtype == 3) {
				ipv4 = ((*pnt & 0x0F) == 4);
				if (ipv4) {
					addressSize = 4;

					// Check for IPv4 address spaces
					pnt = (unsigned char *) buf + i + 4;
					if ((*pnt == 10)  // 10.0.0.0/8
							|| (*pnt == 172 && (*(pnt + 1) & 0xF0) == 1)  // 172.16.0.0/12
							|| (*pnt == 192 && *(pnt + 1) == 168)) {  // 192.168.0.0/16
							for (unsigned int j=0; j<addressSize; j++) {

								pnt = (unsigned char *) buf + i + 4 + j;
								memset	(pnt, 0, 1);

							} // for (unsigned int j=0; j<addressSize; j++)
						i += length;
						continue;
					}
				} else {
					// IPv6
					addressSize = 16;

					// Check for IPv6 address space
					pnt = (unsigned char *) buf + 4;
					if (*pnt == 0xFD) {
						for (unsigned int j=0; j<addressSize; j++) {

							pnt = (unsigned char *) buf + i + 4 + j;
							memset	(pnt, 0, 1);

						} // for (unsigned int j=0; j<addressSize; j++)
						i += length;
						continue;
					}
				}
				for (unsigned int j=0; j<addressSize; j++) {

					pnt = (unsigned char *) buf + i + 4 + j;
					memset	(pnt, anonbytes [*pnt], 1);

				} // for (unsigned int j=0; j<addressSize; j++)
				// if (4 + addressSize < length) {
				// 	// There is also the port to anonymize
				// 	for (unsigned int j=0; j<2; j++) {
				//
				// 		pnt = (unsigned char *) buf + i + 4 + addressSize + j;
				// 		memset	(pnt, anonbytes [*pnt], 1);
				//
				// 	} // for (unsigned int j=0; j<addressSize; j++)
				// }
				// Don't forget to update i
				i += length;
			} else {
				i += length;
			}

		} else if (*pnt == 1) {
			// NO OP
			i++;
		} else if (*pnt == 0) {
			// End of Line option
			break;
		} else {
			// Other option, skip with length
			pnt = (unsigned char *) buf + i + 1;
			i += *pnt;
		}

	} // while(i < len)

	return ANON_RESULT (len);
}
