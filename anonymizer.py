#!/usr/bin/env python3
# Python 3 script to anonymize MPTCP traces
# Author: Quentin De Coninck (quentin.deconinck@uclouvain.be)

import argparse
import os
import shutil
import subprocess

# Some global variables
INFILE = "INFILE"
OUTFILE = "OUTFILE"
PKTANON = "pktanon"
TCPREWRITE = ["tcprewrite"]
TCPREWRITE_OPTIONS = ["--dlt=enet", "--enet-dmac=00:00:00:00:00:00", "--enet-smac=00:00:00:00:00:00"]
SETTINGS_MPTCP_PATH = "profiles/settings_mptcp.xml"
TIMEOUT = 240  # The timeout of the command to pktanon

# Program arguments
parser = argparse.ArgumentParser(description="MPTCP packet anonymizer")
parser.add_argument("input", help="Input directory where traces to be anonymized are located")
parser.add_argument("output", help="Output directory where anonymized traces will be located")

# Parsing the arguments
args = parser.parse_args()

# Expand the path of the directories
input_exp = os.path.abspath(os.path.expanduser(args.input))
output_exp = os.path.abspath(os.path.expanduser(args.output))


def check_directory_exists(directory):
    """ Check if the directory exists, and create it if needed
        If directory is a file, exit the program
    """
    if os.path.exists(directory):
        if not os.path.isdir(directory):
            raise Exception(directory + " is a file: stop", file=sys.stderr)

    else:
        os.makedirs(directory)


def produce_infile(base_filename, in_dirpath):
    """ Generate the INFILE based on the content of base_filename """
    # If it is a pcap.gz file, uncompress it
    if base_filename.endswith(".gz"):
        cmd = ['gunzip', '-c', '-9', os.path.join(in_dirpath, filename)]
        unzipped_file = open(INFILE, 'w')
        if subprocess.call(cmd, stdout=unzipped_file) != 0:
            raise Exception("File " + filename + " could not be unzipped")

        unzipped_file.close()

    elif base_filename.endswith(".pcap"):
        cmd = ['cp', os.path.join(in_dirpath, filename), INFILE]
        if subprocess.call(cmd) != 0:
            raise Exception("File " + filename + " could not be copied")

    else:
        raise Exception(base_filename, "with unrecognized format; skip it")


def clean_infile():
    try:
        os.remove(INFILE)
    except Exception as e:
        print(e)


def rename_outfile(base_filename, out_dirpath):
    base_filename_no_ext = os.path.splitext(base_filename)[0]
    if not base_filename_no_ext.endswith(".pcap"):
        tmp_outfile = base_filename_no_ext + ".pcap"
    else:
        tmp_outfile = base_filename_no_ext

    shutil.move(OUTFILE, tmp_outfile)
    if base_filename.endswith(".gz"):
        cmd = ['gzip', '-c', '-9', tmp_outfile]
        final_gzip_outfile = open(os.path.join(out_dirpath, tmp_outfile + ".gz"), 'w')
        if subprocess.call(cmd, stdout=final_gzip_outfile) != 0:
            raise Exception("File " + tmp_outfile + " could not be compressed")

        final_gzip_outfile.close()

        # Some needed cleaning
        if os.path.exists(tmp_outfile):
            os.remove(tmp_outfile)

    elif base_filename.endswith(".pcap"):
        final_outfile = os.path.join(out_dirpath, tmp_outfile)
        if os.path.exists(final_outfile):
            os.remove(final_outfile)

        shutil.move(tmp_outfile, final_outfile)

    else:
        raise Exception("This should not happen")


def ensure_ethernet_dlt(base_filename):
    tmp_in = INFILE + "_tmp"
    cmd = TCPREWRITE + TCPREWRITE_OPTIONS + ["-i", INFILE, "-o", tmp_in]
    if subprocess.call(cmd) != 0:
        raise Exception("Error when ensuring ethernet data-link type with " + base_filename)

    shutil.move(tmp_in, INFILE)


def anonymize(base_filename):
    cmd = [PKTANON, SETTINGS_MPTCP_PATH]
    if subprocess.call(cmd, timeout=TIMEOUT) != 0:
        raise Exception("Error when anonymizing " + base_filename)


def process_file(filename, in_dirpath, out_dirpath):
    """ Do the actual anonymization on the file with path filename """

    base_filename = os.path.basename(filename)

    try:
        produce_infile(base_filename, in_dirpath)
    except Exception as e:
        print(e)
        return

    # Pktanon only works with ethernet data-link type
    ensure_ethernet_dlt(base_filename)
    anonymize(base_filename)
    clean_infile()
    rename_outfile(base_filename, out_dirpath)


check_directory_exists(output_exp)
for dirpath, dirnames, filenames in os.walk(input_exp, followlinks=True):
    to_copy = dirnames
    new_dirpath = dirpath.replace(input_exp, output_exp)
    for dirname in dirnames:
        fnames = os.listdir(os.path.join(dirpath, dirname))
        for fname in fnames:
            if fname.endswith(".pcap.err.gz"):
                to_copy.remove(dirname)
                break

        for new_dir in to_copy:
            check_directory_exists(os.path.join(new_dirpath, new_dir))

    ok = True
    for filename in filenames:
        if "_lo.pcap" in filename:
            statinfo = os.stat(os.path.join(dirpath, filename))
            if statinfo.st_size <= 1000:
                ok = False
                break

    if not ok:
        continue

    for filename in filenames:
        if filename.endswith(".cfg"):
            shutil.copyfile(os.path.join(dirpath,filename), os.path.join(new_dirpath, filename))
        elif filename.endswith(".pcap.gz") or filename.endswith(".pcap"):
            print("Anonymizing " + filename + "...")
            try:
                process_file(filename, dirpath, new_dirpath)
            except Exception as e:
                print(e)
