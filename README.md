# Packet anonymizer

This repository is a fork of pktanon 1.4.0 which is available [here](http://www.tm.kit.edu/pktanon).

## Install

On Debian-based systems, please ensure that you have the following packets.
```bash
sudo apt-get install libxerces-c-dev libboost-dev tcpreplay
```

On Fedora systems, please ensure that you have the following packets.
```bash
sudo dnf install xerces-c-devel boost-devel tcpreplay
```

This done, you can do the classical recipe.
```bash
./autogen.sh
./configure
make
sudo make install
```

## How to use it?

A Python 3 script `anonymized.py` is available to one-line the process.
Like this, you just need to provide an input folder with the traces to anonymize and an output folder to store the anonymized traces.
From the help of the script:
```
usage: anonymizer.py [-h] input output

MPTCP packet anonymizer

positional arguments:
  input       Input directory where traces to be anonymized are located
  output      Output directory where anonymized traces will be located

optional arguments:
  -h, --help  show this help message and exit
```


## How to control the anonymization power?

The `anonymizer.py` script rely on the profile specified at `profiles/settings_mptcp.xml`.
You can of course modify this file to match your needs, but keep in mind that you should check if your settings are consistents.
For instance, avoid using two different anonymization processes or keys for the source and destination IPs.


## Note about privacy concerns

To avoid any privacy issue, please avoid using the `profiles/settings_mptcp.xml` file with the same hash keys.

## Contact

For more information, please contact Quentin De Coninck at `quentin.deconinck@uclouvain.be`.
